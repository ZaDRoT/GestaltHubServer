using Genie.Router
using FrontendController
using MessagesController
using PostsController
using ProfilesController
using AuthenticationController
using ModeratorsController
using AdminsController

route("/api/") do
  "API v0.0.1"
end

# authenticate
route("/api/auth/") do
  "auth API"
end

route("/api/auth/login", AuthenticationController.login, method = POST)

route("/api/auth/signup", AuthenticationController.signup, method = POST)

route("/api/auth/logout", AuthenticationController.logout, method = GET)

route("/api/auth/whoami", AuthenticationController.whoami, method = GET)

# profile
route("/api/profile/") do
  "profile API"
end

route("/api/profile/getpids", ProfilesController.getPids, method = GET)

route("/api/profile/getinfo", ProfilesController.getInfo, method = GET)

route("/api/profile/changename", ProfilesController.changeName, method = POST)

route("/api/profile/changedescription", ProfilesController.changeDescription, method = POST)

# post
route("/api/post/") do
  "post API"
end

route("/api/post/newpost", PostsController.newPost, method = POST)

route("/api/post/editpost", PostsController.editPost, method = POST)

route("/api/post/removepost", PostsController.removePost, method = POST)

route("/api/post/showpost", PostsController.showPost, method = GET)

route("/api/post/getpostids", PostsController.getPostIds, method = GET)

# message
route("/api/message/") do
  "message API"
end

route("/api/message/newmessage", MessagesController.newMessage, method = POST)

route("/api/message/editmessage", MessagesController.editMessage, method = POST)

route("/api/message/removemessage", MessagesController.removeMessage, method = POST)

route("/api/message/showmessage", MessagesController.showMessage, method = GET)

route("/api/message/getmessageids", MessagesController.getMessageIds, method = GET)

# admin panel
route("/api/admin/") do
  "admin API"
end

route("/api/admin/messages", AdminsController.moderate_messages, method = POST)

# moderator panel
route("/api/moderator/") do
  "moderator API"
end

route("/api/moderator/posts", ModeratorsController.moderate_posts, method = POST)

route("/api/moderator/descriptions", ModeratorsController.moderate_descriptions, method = POST)

route("/", FrontendController.index)

route("/login", FrontendController.login_get, method = GET)
route("/login", FrontendController.login_post, method = POST)

route("/logout", FrontendController.logout)

route("/profile", FrontendController.profile)

route("/profile/edit", FrontendController.editProfile_get, method = GET)
route("/profile/edit", FrontendController.editProfile_post, method = POST)

route("/profiles", FrontendController.profiles)

route("/post/edit", FrontendController.editPost_get, method = GET)
route("/post/edit", FrontendController.editPost_post, method = POST)

route("/post/remove", FrontendController.removePost_get, method = GET)
route("/post/remove", FrontendController.removePost_post, method = POST)

route("/post/new", FrontendController.newPost_get, method = GET)
route("/post/new", FrontendController.newPost_post, method = POST)

route("/posts", FrontendController.posts)

route("/message/edit", FrontendController.editMessage_get, method = GET)
route("/message/edit", FrontendController.editMessage_post, method = POST)

route("/message/remove", FrontendController.removeMessage_get, method = GET)
route("/message/remove", FrontendController.removeMessage_post, method = POST)

route("/message/new", FrontendController.newMessage_get, method = GET)
route("/message/new", FrontendController.newMessage_post, method = POST)

route("/messages", FrontendController.messages)

route("/messages/newmessage", FrontendController.newMessageTarget_get, method = GET)
route("/messages/newmessage", FrontendController.newMessageTarget_post, method = POST)

route("/moderator/posts", FrontendController.moderPosts_get, method = GET)
route("/moderator/posts", FrontendController.moderPosts_post, method = POST)

route("/moderator/descriptions", FrontendController.moderDescriptions_get, method = GET)
route("/moderator/descriptions", FrontendController.moderDescriptions_post, method = POST)

route("/admin/messages", FrontendController.moderMessages_get, method = GET)
route("/admin/messages", FrontendController.moderMessages_post, method = POST)