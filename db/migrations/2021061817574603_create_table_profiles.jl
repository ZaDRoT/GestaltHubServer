module CreateTableProfiles

import SearchLight.Migrations: create_table, column, primary_key, add_index, drop_table

function up()
  create_table(:profiles) do
    [
      primary_key()
      column(:name, :string)
      column(:description, :text)
    ]
  end

end

function down()
  drop_table(:profiles)
end

end
