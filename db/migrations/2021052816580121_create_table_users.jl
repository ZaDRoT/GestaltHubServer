module CreateTableUsers

import SearchLight.Migrations: create_table, column, primary_key, add_index, drop_table

function up()
  create_table(:users) do
    [
      primary_key()
      column(:login, :string)
      column(:password, :string)
    ]
  end

  #add_index(:users, :login)
  #add_index(:users, :password)
end

function down()
  drop_table(:users)
end

end
