module CreateTablePosts

import SearchLight.Migrations: create_table, column, primary_key, add_index, drop_table

function up()
  create_table(:posts) do
    [
      primary_key()
      column(:profile_id, :int)
      column(:content, :text)
    ]
  end

  #add_index(:posts, :profile_id)
end

function down()
  drop_table(:posts)
end

end
