module MessagesController

using Genie, Genie.Router, Genie.Renderer.Json
using SearchLight
using GenieAuthentication
using Messages
using Logging
using Match

function newMessage()
  session = nothing
  user_id = nothing
  message = nothing

  response = Dict{Symbol, Any}(:status=>0, :message=>"Created message")

  try
    @assert length(@params(:POST)) == 3 "Keys numbers"

    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"

    user_id = Genie.Sessions.get(session, :__auth_user_id)
    pids = SearchLight.query("SELECT * FROM pids_by_uid($user_id)")
    @assert parse(UInt, @params(:from)) in pids[:, :pid] "Wrong pid"

    message = Message(to_pid= parse(UInt, @params(:to)), from_pid= parse(UInt, @params(:from)), content= @params(:content))
    @assert save(message) "Save error"
  catch ex
    println(ex)
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end

      AssertionError("Not logged in") =>
        begin 
          response[:status] = 102
          response[:message] = "Not logged in"
        end

      AssertionError("Wrong pid") =>
        begin 
          response[:status] = 103
          response[:message] = "Wrong pid"
        end

      AssertionError("Save error") =>
        begin 
          response[:status] = 104
          response[:message] = "Save error"
        end
    end
  finally
    return json(response)
  end
end

function editMessage()
  session = nothing
  user_id = nothing
  message = nothing

  response = Dict{Symbol, Any}(:status=>0, :message=>"Edited message")

  try
    @assert length(@params(:POST)) == 3 "Keys numbers"

    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"

    user_id = Genie.Sessions.get(session, :__auth_user_id)
    pids = SearchLight.query("SELECT * FROM pids_by_uid($user_id)")
    @assert parse(UInt, @params(:pid)) in pids[:, :pid] "Wrong pid"

    message_ids = SearchLight.query("SELECT * FROM messages WHERE from_pid = ($(@params(:pid)))")
    @assert parse(UInt, @params(:message_id)) in message_ids[:, :id] "Wrong message_id"

    message = SearchLight.findone(Message, id = parse(UInt, @params(:message_id)))

    message.content = @params(:content)
    @assert save(message) "Save error"
  catch ex
    println(ex)
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end

      AssertionError("Not logged in") =>
        begin 
          response[:status] = 102
          response[:message] = "Not logged in"
        end

      AssertionError("Wrong pid") =>
        begin 
          response[:status] = 103
          response[:message] = "Wrong pid"
        end

      AssertionError("Wrong message_id") =>
        begin 
          response[:status] = 104
          response[:message] = "Wrong message_id"
        end

      AssertionError("Save error") =>
        begin 
          response[:status] = 105
          response[:message] = "Save error"
        end
    end
  finally
    return json(response)
  end
end

function removeMessage()
  session = nothing
  user_id = nothing
  message = nothing

  response = Dict{Symbol, Any}(:status=>0, :message=>"Deleted message")

  try
    @assert length(@params(:POST)) == 2 "Keys numbers"

    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"

    user_id = Genie.Sessions.get(session, :__auth_user_id)
    pids = SearchLight.query("SELECT * FROM pids_by_uid($user_id)")
    @assert parse(UInt, @params(:pid)) in pids[:, :pid] "Wrong pid"

    message_ids = SearchLight.query("SELECT * FROM messages WHERE from_pid = ($(@params(:pid)))")
    @assert parse(UInt, @params(:message_id)) in message_ids[:, :id] "Wrong message_id"

    message = SearchLight.findone(Message, id = parse(UInt, @params(:message_id)))
    @assert delete(message) "Delete error"
  catch ex
    println(ex)
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end

      AssertionError("Not logged in") =>
        begin 
          response[:status] = 102
          response[:message] = "Not logged in"
        end

      AssertionError("Wrong pid") =>
        begin 
          response[:status] = 103
          response[:message] = "Wrong pid"
        end

      AssertionError("Wrong message_id") =>
        begin 
          response[:status] = 104
          response[:message] = "Wrong message_id"
        end

      AssertionError("Delete error") =>
        begin 
          response[:status] = 105
          response[:message] = "Delete error"
        end
    end
  finally
    return json(response)
  end
end

function showMessage()
  session = nothing
  user_id = nothing
  message = nothing

  response = Dict{Symbol, Any}(:status=>0, :content=>nothing, :from=>nothing, :to=>nothing, :message=>"Showed message")

  try
    println(length(@params(:GET)))
    @assert length(@params(:GET)) >= 1 "Keys numbers"

    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"

    user_id = Genie.Sessions.get(session, :__auth_user_id)
    pids = SearchLight.query("SELECT * FROM pids_by_uid($user_id)")
    @assert parse(UInt, @params(:pid)) in pids[:, :pid] "Wrong pid"

    message_ids = SearchLight.query("SELECT * FROM messages WHERE (from_pid = ($(@params(:pid)))) OR (to_pid = ($(@params(:pid))))")
    @assert (parse(UInt, @params(:message_id)) in message_ids[:, :id]) "Wrong message_ids"

    message = SearchLight.findone(Message, id = parse(UInt, @params(:message_id)))
    response[:content] = message.content
    response[:from] = message.from_pid
    response[:to] = message.to_pid
  catch ex
    println(ex)
    @match ex begin
      AssertionError("Not logged in") =>
        begin 
          response[:status] = 101
          response[:message] = "Not logged in"
        end

      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 102
          response[:message] = "Wrong keys numbers"
        end

      AssertionError("Wrong pid") =>
        begin 
          response[:status] = 103
          response[:message] = "Wrong pid"
        end

      AssertionError("Wrong message_ids") =>
        begin 
          response[:status] = 104
          response[:message] = "Wrong message ID"
        end
    end
  finally
    return response
  end
end

function getMessageIds()
  session = nothing
  user_id = nothing
  message_ids = nothing

  response = Dict{Symbol, Any}(:status=>0, :message_ids=>[], :message=>"Got message IDs")

  try
    println(length(@params(:GET)))
    @assert length(@params(:GET)) >= 1 "Keys numbers"

    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"

    user_id = Genie.Sessions.get(session, :__auth_user_id)
    pids = SearchLight.query("SELECT * FROM pids_by_uid($user_id)")
    @assert parse(UInt, @params(:pid)) in pids[:, :pid] "Wrong pid"

    message_ids = SearchLight.query("SELECT * FROM messages WHERE (from_pid = ($(@params(:pid)))) OR (to_pid = ($(@params(:pid))))")
    for message_id in message_ids[:, :id]
      append!(response[:message_ids], message_id)
    end
  catch ex
    println(ex)
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end

      AssertionError("Not logged in") =>
        begin 
          response[:status] = 102
          response[:message] = "Not logged in"
        end

      AssertionError("Wrong pid") =>
        begin 
          response[:status] = 103
          response[:message] = "Wrong pid"
        end
    end
  finally
    return json(response)
  end
end

end
