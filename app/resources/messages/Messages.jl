module Messages

import SearchLight: AbstractModel, DbId, Validation
import Base: @kwdef
using MessagesValidator

export Message

@kwdef mutable struct Message <: AbstractModel
  id::DbId = DbId()
  to_pid::UInt = 0
  from_pid::UInt = 0
  content::String = ""
end

Validation.validator(u::Type{Message}) = Validation.ModelValidator([
  Validation.ValidationRule(:to_pid, MessagesValidator.not_empty),
  Validation.ValidationRule(:from_pid, MessagesValidator.not_empty),
  Validation.ValidationRule(:content, MessagesValidator.not_empty)
])

end
