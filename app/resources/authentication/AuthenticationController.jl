module AuthenticationController

using Genie, Genie.Router, Genie.Renderer.Json, Genie.Renderer
using HTTP
using SearchLight
using GenieAuthentication
using Users
using Logging
using Match

function login()
  login = nothing
  password = nothing
  user = nothing
  session = nothing

  response = Dict{Symbol, Any}(:status=>0, :message=>"login successed")

  try
    @assert length(@params(:POST)) == 2 "Keys numbers"

    login = @params(:login)
    password = Users.hash_password(@params(:password))

    user = SearchLight.findone(User, login= login, password= password)
    @assert !(isnothing(user)) "Non valid"

    session = Genie.Sessions.session(@params)

    GenieAuthentication.authenticate(user.id, session)
  catch ex
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end

      KeyError(:login) =>
        begin 
          response[:status] = 102
          response[:message] = "No login key"
        end

      KeyError(:password) =>
        begin 
          response[:status] = 103
          response[:message] = "No password key"
        end

      AssertionError("Non valid") =>
        begin 
          response[:status] = 104
          response[:message] = "Non valid credentials"
        end
    end
  finally
    return json(response)
  end
end

function logout()
  session = nothing

  response = Dict{Symbol, Any}(:status=>0, :message=>"Logout successed")

  try
    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"
    
    GenieAuthentication.deauthenticate(session)
  catch ex
    @match ex begin
      AssertionError("Not logged in") =>
        begin 
          response[:status] = 101
          response[:message] = "Not logged in"
        end
    end
  finally
    return json(response)
  end
end

function signup()
  login = nothing
  password = nothing
  user = nothing

  response = Dict{Symbol, Any}(:status=>0, :login=>nothing, :message=>"Signup successed")

  try
    @assert length(@params(:POST)) == 2 "Keys numbers"

    login = @params(:login)
    password = @params(:password)

    user = User(login= login, password= (password |> Users.hash_password))
    @assert save(user) "Login taken"

    response[:login] = user.login
  catch ex
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end

      KeyError(:login) =>
        begin 
          response[:status] = 102
          response[:message] = "No login key"
        end

      KeyError(:password) =>
        begin 
          response[:status] = 103
          response[:message] = "No password key"
        end

      AssertionError("Login taken") =>
        begin 
          response[:status] = 103
          response[:message] = "Login already taken"
        end
    end
  finally
    json(response)
  end
end

function whoami()
  session = nothing
  user = nothing

  response = Dict{Symbol, Any}(:status=>0, :login=>nothing, :message=>"Whoami successed")

  try
    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"

    user = SearchLight.findone(User, id = Genie.Sessions.get(session, :__auth_user_id))
    response[:login] = user.login
  catch ex
    @match ex begin
      AssertionError("Not logged in") =>
        begin 
          response[:status] = 101
          response[:message] = "Not logged in"
        end
    end
  finally
    return json(response)
  end
end

end