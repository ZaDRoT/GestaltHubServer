module PostsController
  
using Genie, Genie.Router, Genie.Renderer.Json
using SearchLight
using GenieAuthentication
using Posts
using Logging
using Match

function newPost()
  session = nothing
  user_id = nothing
  post = nothing

  response = Dict{Symbol, Any}(:status=>0, :message=>"Created post")

  try
    @assert length(@params(:POST)) == 2 "Keys numbers"

    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"

    user_id = Genie.Sessions.get(session, :__auth_user_id)
    pids = SearchLight.query("SELECT * FROM pids_by_uid($user_id)")
    @assert parse(UInt, @params(:pid)) in pids[:, :pid] "Wrong pid"

    post = Post(profile_id= parse(UInt, @params(:pid)), content= @params(:content))
    @assert save(post) "Save error"
  catch ex
    println(ex)
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end

      AssertionError("Not logged in") =>
        begin 
          response[:status] = 102
          response[:message] = "Not logged in"
        end

      AssertionError("Wrong pid") =>
        begin 
          response[:status] = 103
          response[:message] = "Wrong pid"
        end

      AssertionError("Save error") =>
        begin 
          response[:status] = 104
          response[:message] = "Save error"
        end
    end
  finally
    return json(response)
  end
end

function editPost()
  session = nothing
  user_id = nothing
  post = nothing

  response = Dict{Symbol, Any}(:status=>0, :message=>"Edited post")

  try
    @assert length(@params(:POST)) == 3 "Keys numbers"

    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"

    user_id = Genie.Sessions.get(session, :__auth_user_id)
    pids = SearchLight.query("SELECT * FROM pids_by_uid($user_id)")
    @assert parse(UInt, @params(:pid)) in pids[:, :pid] "Wrong pid"

    post_ids = SearchLight.query("SELECT * FROM posts WHERE profile_id = ($(@params(:pid)))")
    @assert parse(UInt, @params(:post_id)) in post_ids[:, :id] "Wrong post_id"

    post = SearchLight.findone(Post, id = parse(UInt, @params(:post_id)))

    post.content = @params(:content)
    @assert save(post) "Save error"
  catch ex
    println(ex)
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end

      AssertionError("Not logged in") =>
        begin 
          response[:status] = 102
          response[:message] = "Not logged in"
        end

      AssertionError("Wrong pid") =>
        begin 
          response[:status] = 103
          response[:message] = "Wrong pid"
        end

      AssertionError("Wrong post_id") =>
        begin 
          response[:status] = 104
          response[:message] = "Wrong post_id"
        end

      AssertionError("Save error") =>
        begin 
          response[:status] = 105
          response[:message] = "Save error"
        end
    end
  finally
    return json(response)
  end
end

function removePost()
  session = nothing
  user_id = nothing
  post = nothing

  response = Dict{Symbol, Any}(:status=>0, :message=>"Deleted post")

  try
    @assert length(@params(:POST)) == 2 "Keys numbers"

    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"

    user_id = Genie.Sessions.get(session, :__auth_user_id)
    pids = SearchLight.query("SELECT * FROM pids_by_uid($user_id)")
    @assert parse(UInt, @params(:pid)) in pids[:, :pid] "Wrong pid"

    post_ids = SearchLight.query("SELECT * FROM posts WHERE profile_id = ($(@params(:pid)))")
    @assert parse(UInt, @params(:post_id)) in post_ids[:, :id] "Wrong post_id"

    post = SearchLight.findone(Post, id = parse(UInt, @params(:post_id)))
    @assert delete(post) "Delete error"
  catch ex
    println(ex)
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end

      AssertionError("Not logged in") =>
        begin 
          response[:status] = 102
          response[:message] = "Not logged in"
        end

      AssertionError("Wrong pid") =>
        begin 
          response[:status] = 103
          response[:message] = "Wrong pid"
        end

      AssertionError("Wrong post_id") =>
        begin 
          response[:status] = 104
          response[:message] = "Wrong post_id"
        end

      AssertionError("Delete error") =>
        begin 
          response[:status] = 105
          response[:message] = "Delete error"
        end
    end
  finally
    return json(response)
  end
end

function showPost()
  post = nothing

  response = Dict{Symbol, Any}(:status=>0, :content=>nothing, :pid=>nothing, :message=>"Showed post")

  try
    @assert length(@params(:GET)) == 1 "Keys numbers"

    post_ids = SearchLight.query("SELECT * FROM posts")
    @assert parse(UInt, @params(:post_id)) in post_ids[:, :id] "Wrong post_id"

    post = SearchLight.findone(Post, id = parse(UInt, @params(:post_id)))
    response[:content] = post.content
    response[:pid] = post_ids[findfirst(x->x==parse(UInt, @params(:post_id)), post_ids[:, :id]), :profile_id]
    println(response[:pid])
  catch ex
    println(ex)
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end

      AssertionError("Wrong post_id") =>
        begin 
          response[:status] = 104
          response[:message] = "Wrong post_id"
        end

      AssertionError("Delete error") =>
        begin 
          response[:status] = 105
          response[:message] = "Delete error"
        end
    end
  finally
    return json(response)
  end
end

function getPostIds()
  post_ids = nothing

  response = Dict{Symbol, Any}(:status=>0, :post_ids=>[], :message=>"Got post_ids")

  try
    @assert length(@params(:GET)) == 1 "Keys numbers"

    post_ids = SearchLight.query("SELECT * FROM posts WHERE profile_id = $(@params(:pid))")
    for post_id in post_ids[:, :id]
      append!(response[:post_ids], post_id)
    end
  catch ex
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end
    end
  finally
    return json(response)
  end
end

end
