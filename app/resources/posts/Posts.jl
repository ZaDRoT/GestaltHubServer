module Posts

import SearchLight: AbstractModel, DbId, Validation
import Base: @kwdef
using PostsValidator

export Post

@kwdef mutable struct Post <: AbstractModel
  id::DbId = DbId()
  profile_id::UInt = 0
  content::String = ""
end

Validation.validator(u::Type{Post}) = Validation.ModelValidator([
  Validation.ValidationRule(:profile_id, PostsValidator.not_empty),
  Validation.ValidationRule(:content, PostsValidator.not_empty)
])

end
