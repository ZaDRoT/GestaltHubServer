module AdminsController

using Genie, Genie.Router, Genie.Renderer.Json
using SearchLight
using GenieAuthentication
using Logging
using Match

function moderate_messages()
  response = Dict{Symbol, Any}(:status=>0, :messages=>[], :message=>"Moderated messages")

  try
    @assert (@params(:login) == "admin" && @params(:password) == "admin000000") "Not logged in"
    SearchLight.connection()[:user] = @params(:login)
    SearchLight.connection()[:password] = @params(:password)

    if haskey(@params(), :message_id)
      message_id = parse(UInt, @params(:message_id))
      SearchLight.query("DELETE FROM messages WHERE id = $message_id")
    end
    
    messages = SearchLight.query("SELECT * FROM messages")
    for message in zip(messages[:, :id], messages[:, :to_pid], messages[:, :from_pid], messages[:, :content])
      println(message)
      push!(response[:messages], [message[1], message[2], message[3], message[4]])
    end
  catch ex
    @match ex begin
      AssertionError("Not logged in") =>
        begin 
          response[:status] = 101
          response[:message] = "Not logged in"
        end
    end
  finally
    return json(response)
  end
end

end
