module ModeratorsController

using Genie, Genie.Router, Genie.Renderer.Json
using SearchLight
using GenieAuthentication
using Logging
using Match

function moderate_descriptions()
  response = Dict{Symbol, Any}(:status=>0, :profiles=>[], :message=>"Moderated profiles")

  try
    @assert (@params(:login) == "moderator" && @params(:password) == "moderator000000") "Not logged in"
    SearchLight.connection()[:user] = @params(:login)
    SearchLight.connection()[:password] = @params(:password)

    if haskey(@params(), :profile_id)
      profile_id = parse(UInt, @params(:profile_id))
      SearchLight.query("UPDATE profiles SET description = 'change' WHERE id = $profile_id")
    end
    
    profiles = SearchLight.query("SELECT * FROM profiles")
    for profile in zip(profiles[:, :id], profiles[:, :name], profiles[:, :description])
      println(profile)
      push!(response[:profiles], [profile[1], profile[2], profile[3]])
    end
    println(response)
  catch ex
    @match ex begin
      AssertionError("Not logged in") =>
        begin 
          response[:status] = 101
          response[:message] = "Not logged in"
        end
    end
  finally
    return json(response)
  end
end

function moderate_posts()
  response = Dict{Symbol, Any}(:status=>0, :posts=>[], :message=>"Moderated messages")

  try
    @assert (@params(:login) == "moderator" && @params(:password) == "moderator000000") "Not logged in"
    SearchLight.connection()[:user] = @params(:login)
    SearchLight.connection()[:user] = @params(:password)

    if haskey(@params(), :post_id)
      post_id = parse(UInt, @params(:post_id))
      SearchLight.query("DELETE FROM posts WHERE id = $post_id")
    end
    
    posts = SearchLight.query("SELECT * FROM posts")
    for post in zip(posts[:, :id], posts[:, :profile_id], posts[:, :content])
      push!(response[:posts], [post[1], post[2], post[3]])
    end
  catch ex
    @match ex begin
      AssertionError("Not logged in") =>
        begin 
          response[:status] = 101
          response[:message] = "Not logged in"
        end
    end
  finally
    return json(response)
  end
end

end
