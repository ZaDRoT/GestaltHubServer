module Users

using SearchLight, SearchLight.Validation, UsersValidator
using SHA

export User

Base.@kwdef mutable struct User <: AbstractModel
  id::DbId = DbId()
  login::String = ""
  password::String = ""
end

Validation.validator(u::Type{User}) = ModelValidator([
  ValidationRule(:login, UsersValidator.not_empty),
  ValidationRule(:login, UsersValidator.unique),
  ValidationRule(:password, UsersValidator.not_empty)
])

function hash_password(password::String)
  sha256(password) |> bytes2hex
end

end