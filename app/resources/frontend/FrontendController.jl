module FrontendController

using Genie, Genie.Renderer, Genie.Requests, JSON

using MessagesController
using PostsController
using ProfilesController
using AuthenticationController
using ModeratorsController
using AdminsController


############################
# Как-то вынести код ниже

function addToGetRequest(key, value)
  merge!(get(task_local_storage(:__params), :GET, nothing), Dict(key=>value))
  @params(:pid) = value
end

function addToPostRequest(key, value)
  merge!(get(task_local_storage(:__params), :POST, nothing), Dict(key=>value))
  @params(key) = value
end

###########################

function index()
  whoami = get(JSON.parse(String(AuthenticationController.whoami().body)), "login", nothing)

  pids = get(JSON.parse(String(ProfilesController.getPids().body)), "pids", nothing)
  profiles = []
  for pid in pids
    merge!(get(task_local_storage(:__params), :GET, nothing), Dict(:pid=>string(pid)))
    @params(:pid) = string(pid)
    profileName = get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing)
    push!(profiles, [pid, profileName])
  end

  Renderer.Html.html(:frontend, :index, whoami=whoami, profiles=profiles)
end

function login_get()
  Renderer.Html.html(:frontend, :login)
end

function login_post()
  AuthenticationController.login()
  Renderer.redirect("/")
end

function logout()
  AuthenticationController.logout()
  Renderer.redirect("/")
end

function profile()
  whoami = get(JSON.parse(String(AuthenticationController.whoami().body)), "login", nothing)

  pids = get(JSON.parse(String(ProfilesController.getPids().body)), "pids", nothing)
  profiles = []
  tmp_pid = @params(:pid)
  for pid in pids
    merge!(get(task_local_storage(:__params), :GET, nothing), Dict(:pid=>string(pid)))
    @params(:pid) = string(pid)
    profileName = get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing)
    push!(profiles, [pid, profileName])
  end
  @params(:pid) = tmp_pid
  info = [get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing), get(JSON.parse(String(ProfilesController.getInfo().body)), "description", nothing)]
  
  Renderer.Html.html(:frontend, :profile, pid=parse(UInt, @params(:pid)), pids=pids, info=info, whoami=whoami, profiles=profiles)
end

function editProfile_get()
  whoami = get(JSON.parse(String(AuthenticationController.whoami().body)), "login", nothing)

  pids = get(JSON.parse(String(ProfilesController.getPids().body)), "pids", nothing)
  profiles = []
  tmp_pid = @params(:pid)
  for pid in pids
    merge!(get(task_local_storage(:__params), :GET, nothing), Dict(:pid=>string(pid)))
    @params(:pid) = string(pid)
    profileName = get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing)
    push!(profiles, [pid, profileName])
  end
  @params(:pid) = tmp_pid
  
  Renderer.Html.html(:frontend, :editprofile, whoami=whoami, profiles=profiles, pid=parse(UInt, @params(:pid)))
end

function editProfile_post()
  if ! isempty(@params(:name))
    ProfilesController.changeName()
  end
  if ! isempty(@params(:description))
    ProfilesController.changeDescription()
  end
  Renderer.redirect("/profile?pid=$(@params(:pid))")
end

function profiles()
  whoami = get(JSON.parse(String(AuthenticationController.whoami().body)), "login", nothing)

  pids = get(JSON.parse(String(ProfilesController.getPids().body)), "pids", nothing)
  profiles = []
  for pid in pids
    merge!(get(task_local_storage(:__params), :GET, nothing), Dict(:pid=>string(pid)))
    @params(:pid) = string(pid)
    profileName = get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing)
    push!(profiles, [pid, profileName])
  end
  allProfiles = get(JSON.parse(String(ProfilesController.getAllProfiles().body)), "profiles", nothing)
  Renderer.Html.html(:frontend, :profiles, whoami=whoami, profiles=profiles, allProfiles=allProfiles)
end

function post()
  content = get(JSON.parse(String(PostsController.showPost().body)), "content", nothing)
  
  Renderer.Html.html(:frontend, :post, content=content)
end

function editPost_get()
  whoami = get(JSON.parse(String(AuthenticationController.whoami().body)), "login", nothing)

  pids = get(JSON.parse(String(ProfilesController.getPids().body)), "pids", nothing)
  profiles = []
  tmp_pid = @params(:pid)
  post_id = @params(:post_id)
  pop!(get(task_local_storage(:__params), :GET, nothing), :post_id)
  for pid in pids
    merge!(get(task_local_storage(:__params), :GET, nothing), Dict(:pid=>string(pid)))
    @params(:pid) = string(pid)
    profileName = get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing)
    push!(profiles, [pid, profileName])
  end

  Renderer.Html.html(:frontend, :editpost, whoami=whoami, profiles=profiles, post_id=parse(UInt, post_id), pid=parse(UInt, tmp_pid))
end

function editPost_post()
  if ! isempty(@params(:content))
    PostsController.editPost()
  end

  Renderer.redirect("/posts?pid=$(@params(:pid))")
end

function newPost_get() 
  "Сonstruction works"
end

function newPost_post()
  if ! isempty(@params(:content))
    PostsController.newPost()
  end
 
  Renderer.redirect("/posts?pid=$(@params(:pid))")
end

function removePost_get()
  Renderer.Html.html(:frontend, :removepost, post_id=parse(UInt, @params(:post_id)), pid=parse(UInt, @params(:pid)))
end

function removePost_post()
  PostsController.removePost()

  Renderer.redirect("/posts?pid=$(@params(:pid))")
end

function posts()
  whoami = get(JSON.parse(String(AuthenticationController.whoami().body)), "login", nothing)
  
  pid = @params(:pid)
  
  pids = get(JSON.parse(String(ProfilesController.getPids().body)), "pids", nothing)
  profiles = []
  for pid in pids
    merge!(get(task_local_storage(:__params), :GET, nothing), Dict(:pid=>string(pid)))
    @params(:pid) = string(pid)
    profileName = get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing)
    push!(profiles, [pid, profileName])
  end
  @params(:pid) = pid

  info = [get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing), get(JSON.parse(String(ProfilesController.getInfo().body)), "description", nothing)]

  post_ids = get(JSON.parse(String(PostsController.getPostIds().body)), "post_ids", nothing)
  #pop!(@params(), :pid)
  pop!(get(task_local_storage(:__params), :GET, nothing), :pid)

  posts = []
  for post_id in post_ids
    merge!(get(task_local_storage(:__params), :GET, nothing), Dict(:post_id=>string(post_id)))
    @params(:post_id) = string(post_id)
    post = get(JSON.parse(String(PostsController.showPost().body)), "content", nothing)
    push!(posts, [post_id, post])
  end

  #####
  # Баг в Genie с макросом рендера, не забыть создать ишу, ниже костыль. (в шаблоне обработка костыля)
  if isempty(posts)
    push!(posts, [0])
  end
  #####
  posts = reverse(posts)

  Renderer.Html.html(:frontend, :posts, pid=parse(UInt, @params(:pid)), pids=pids, info=info, whoami=whoami, profiles=profiles, posts=posts)
end
###########

function editMessage_get()
  whoami = get(JSON.parse(String(AuthenticationController.whoami().body)), "login", nothing)

  pids = get(JSON.parse(String(ProfilesController.getPids().body)), "pids", nothing)
  profiles = []
  tmp_pid = @params(:pid)
  message_id = @params(:message_id)
  pop!(get(task_local_storage(:__params), :GET, nothing), :message_id)
  for pid in pids
    merge!(get(task_local_storage(:__params), :GET, nothing), Dict(:pid=>string(pid)))
    @params(:pid) = string(pid)
    profileName = get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing)
    push!(profiles, [pid, profileName])
  end

  Renderer.Html.html(:frontend, :editmessage, whoami=whoami, profiles=profiles, message_id=parse(UInt, message_id), pid=parse(UInt, tmp_pid))
end

function editMessage_post()
  if ! isempty(@params(:content))
    MessagesController.editMessage()
  end

  Renderer.redirect("/messages?pid=$(@params(:pid))")
end

function newMessage_get() 
  whoami = get(JSON.parse(String(AuthenticationController.whoami().body)), "login", nothing)

  to = @params(:to)
  pop!(get(task_local_storage(:__params), :GET, nothing), :to)

  pids = get(JSON.parse(String(ProfilesController.getPids().body)), "pids", nothing)
  profiles = []
  for pid in pids
    merge!(get(task_local_storage(:__params), :GET, nothing), Dict(:pid=>string(pid)))
    @params(:pid) = string(pid)
    profileName = get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing)
    push!(profiles, [pid, profileName])
  end

  Renderer.Html.html(:frontend, :newmessage, whoami=whoami, profiles=profiles, to=to)
end

function newMessage_post()
  if ! isempty(@params(:content))
    MessagesController.newMessage()
  end
 
  Renderer.redirect("/messages?pid=$(@params(:from))")
end

function removeMessage_get()
  Renderer.Html.html(:frontend, :removemessage, message_id=parse(UInt, @params(:message_id)), pid=parse(UInt, @params(:pid)))
end

function removeMessage_post()
  MessagesController.removeMessage()

  Renderer.redirect("/messages?pid=$(@params(:pid))")
end

function messages()
  whoami = get(JSON.parse(String(AuthenticationController.whoami().body)), "login", nothing)
  pid = @params(:pid)

  pids = get(JSON.parse(String(ProfilesController.getPids().body)), "pids", nothing)
  profiles = []
  for pid in pids
    merge!(get(task_local_storage(:__params), :GET, nothing), Dict(:pid=>string(pid)))
    @params(:pid) = string(pid)
    profileName = get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing)
    push!(profiles, [pid, profileName])
  end
  @params(:pid) = pid

  info = [get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing), get(JSON.parse(String(ProfilesController.getInfo().body)), "description", nothing)]

  message_ids = get(JSON.parse(String(MessagesController.getMessageIds().body)), "message_ids", nothing)

  messages = []
  for mid in message_ids
    merge!(get(task_local_storage(:__params), :GET, nothing), Dict(:message_id=>string(mid)))
    @params(:message_id) = string(mid)
    response = MessagesController.showMessage()
    content = get(response, :content, nothing)
    to = get(response, :to, nothing)
    from = get(response, :from, nothing)
    @params(:pid) = string(to)
    to_name = get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing)
    @params(:pid) = string(from)
    from_name = get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing)
    push!(messages, [mid, content, to, to_name, from, from_name])
  end
  messages = reverse(messages)

  @params(:pid) = pid

  Renderer.Html.html(:frontend, :messages, pid=parse(UInt, @params(:pid)), pids=pids, info=info, whoami=whoami, messages=messages, profiles=profiles)
end

function newMessageTarget_get() 
  whoami = get(JSON.parse(String(AuthenticationController.whoami().body)), "login", nothing)

  to = @params(:to)
  from = @params(:from)
  pop!(get(task_local_storage(:__params), :GET, nothing), :to)
  pop!(get(task_local_storage(:__params), :GET, nothing), :from)

  pids = get(JSON.parse(String(ProfilesController.getPids().body)), "pids", nothing)
  profiles = []
  for pid in pids
    merge!(get(task_local_storage(:__params), :GET, nothing), Dict(:pid=>string(pid)))
    @params(:pid) = string(pid)
    profileName = get(JSON.parse(String(ProfilesController.getInfo().body)), "name", nothing)
    push!(profiles, [pid, profileName])
  end

  Renderer.Html.html(:frontend, :newmessagetarget, whoami=whoami, profiles=profiles, to=to, from=from)
end

function newMessageTarget_post()
  if ! isempty(@params(:content))
    MessagesController.newMessage()
  end
 
  Renderer.redirect("/messages?pid=$(@params(:from))")
end

function moderPosts_get()
  login = "login"
  if (haskey(@params(), :login))
    login = @params(:login)
  end

  password = "password"
  if (haskey(@params(), :password))
    password = @params(:password)
  end

  posts = nothing
  if ! isnothing(login) && ! isnothing(password)
    merge!(get(task_local_storage(:__params), :POST, nothing), Dict(:login=>login))
    @params(:login) = login
    merge!(get(task_local_storage(:__params), :POST, nothing), Dict(:password=>password))
    @params(:password) = password
    posts = get(JSON.parse(String(ModeratorsController.moderate_posts().body)), "posts", nothing)
  end
  if isempty(posts) || isnothing(profiles)
    posts = [[0, 0, ""]]
  end

  Renderer.Html.html(:frontend, :moderposts, login=login, password=password, posts=posts)
end

function moderPosts_post()
  if ! isempty(@params(:post_id)) && ! isempty(@params(:login)) && ! isempty(@params(:password))
    ModeratorsController.moderate_posts()
  end
 
  if ! isempty(@params(:login)) && ! isempty(@params(:password))
    Renderer.redirect("/moderator/posts?login=$(@params(:login))&password=$(@params(:password))")
  else
    Renderer.redirect("/moderator/posts")
  end
end

function moderDescriptions_get()
  login = ""
  if (haskey(@params(), :login))
    login = @params(:login)
  end

  password = ""
  if (haskey(@params(), :password))
    password = @params(:password)
  end

  profiles = nothing
  if ! isnothing(login) && ! isnothing(password)
    merge!(get(task_local_storage(:__params), :POST, nothing), Dict(:login=>login))
    @params(:login) = login
    merge!(get(task_local_storage(:__params), :POST, nothing), Dict(:password=>password))
    @params(:password) = password
    profiles = get(JSON.parse(String(ModeratorsController.moderate_descriptions().body)), "profiles", nothing)
  end
  if isempty(profiles) || isnothing(profiles)
    profiles = [[0, "", ""]]
  end

  Renderer.Html.html(:frontend, :moderdescriptions, login=login, password=password, profiles=profiles)
end

function moderDescriptions_post()
  if ! isempty(@params(:profile_id)) && ! isempty(@params(:login)) && ! isempty(@params(:password))
    ModeratorsController.moderate_descriptions()
  end
 
  if ! isempty(@params(:login)) && ! isempty(@params(:password))
    Renderer.redirect("/moderator/descriptions?login=$(@params(:login))&password=$(@params(:password))")
  else
    Renderer.redirect("/moderator/descriptions")
  end
end

function moderMessages_get()
  login = ""
  if (haskey(@params(), :login))
    login = @params(:login)
  end

  password = ""
  if (haskey(@params(), :password))
    password = @params(:password)
  end

  messages = nothing
  if ! isnothing(login) && ! isnothing(password)
    merge!(get(task_local_storage(:__params), :POST, nothing), Dict(:login=>login))
    @params(:login) = login
    merge!(get(task_local_storage(:__params), :POST, nothing), Dict(:password=>password))
    @params(:password) = password
    messages = get(JSON.parse(String(AdminsController.moderate_messages().body)), "messages", nothing)
  end
  if isempty(messages) || isnothing(messages)
    messages = [[0, 0, 0, ""]]
  end

  Renderer.Html.html(:frontend, :modermessages, login=login, password=password, messages=messages)
end

function moderMessages_post()
  if ! isempty(@params(:message_id)) && ! isempty(@params(:login)) && ! isempty(@params(:password))
    AdminsController.moderate_messages()
  end
 
  if ! isempty(@params(:login)) && ! isempty(@params(:password))
    Renderer.redirect("/admin/messages?login=$(@params(:login))&password=$(@params(:password))")
  else
    Renderer.redirect("/admin/messages")
  end
end

end