module ProfilesController

using Genie, Genie.Router, Genie.Renderer.Json
using SearchLight
using GenieAuthentication
using Profiles
using Logging
using Match

function getPids()
  session = nothing
  user_id = nothing
  pids = nothing
  profile = nothing

  response = Dict{Symbol, Any}(:status=>0, :pids=>[], :message=>"Got pids")

  try
    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"

    user_id = Genie.Sessions.get(session, :__auth_user_id)
    pids = SearchLight.query("SELECT * FROM pids_by_uid($user_id)")
    for pid in pids[:, :pid]
      append!(response[:pids], pid)
    end
  catch ex
    @match ex begin
      AssertionError("Not logged in") =>
        begin 
          response[:status] = 101
          response[:message] = "Not logged in"
        end
    end
  finally
    return json(response)
  end
end

function getInfo()
  session = nothing
  user_id = nothing
  pids = nothing
  profile = nothing

  response = Dict{Symbol, Any}(:status=>0, :name=>nothing, :description=>nothing, :message=>"Got info")

  try
    println(length(@params(:GET)))
    @assert length(@params(:GET)) >= 1 "Keys numbers"
    #session = Genie.Sessions.session(@params)
    #@assert GenieAuthentication.is_authenticated(session) "Not logged in"

    #user_id = Genie.Sessions.get(session, :__auth_user_id)
    #pids = SearchLight.query("SELECT * FROM pids_by_uid($user_id)")
    #@assert parse(UInt, @params(:pid)) in pids[:, :pid] "Wrong pid"

    profile = SearchLight.findone(Profile, id = @params(:pid))

    response[:name] = profile.name
    response[:description] = profile.description
  catch ex
    println(ex)
    @match ex begin
      AssertionError("Not logged in") =>
        begin 
          response[:status] = 101
          response[:message] = "Not logged in"
        end
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 102
          response[:message] = "Wrong keys numbers"
        end
      AssertionError("Wrong pid") =>
        begin 
          response[:status] = 103
          response[:message] = "Wrong pid"
        end
      KeyError(:pid) =>
        begin 
          response[:status] = 104
          response[:message] = "No pid key"
        end
    end
  finally
    return json(response)
  end
end

function changeName()
  session = nothing
  user_id = nothing
  pids = nothing
  profile = nothing

  response = Dict{Symbol, Any}(:status=>0, :message=>"Changed name")

  try
    @assert length(@params(:POST)) >= 2 "Keys numbers"

    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"

    user_id = Genie.Sessions.get(session, :__auth_user_id)
    pids = SearchLight.query("SELECT * FROM pids_by_uid($user_id)")
    @assert parse(UInt, @params(:pid)) in pids[:, :pid] "Wrong pid"

    profile = SearchLight.findone(Profile, id = @params(:pid))

    profile.name = @params(:name)
    save!(profile)
  catch ex
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end

      KeyError(:name) =>
        begin 
          response[:status] = 102
          response[:message] = "No name key"
        end

      AssertionError("Not logged in") =>
        begin 
          response[:status] = 103
          response[:message] = "Not logged in"
        end
      AssertionError("Wrong pid") =>
        begin 
          response[:status] = 104
          response[:message] = "Wrong pid"
        end
      KeyError(:pid) =>
        begin 
          response[:status] = 105
          response[:message] = "No pid key"
        end
    end
  finally
    return json(response)
  end
end

function changeDescription()
  session = nothing
  user_id = nothing
  pids = nothing
  profile = nothing

  response = Dict{Symbol, Any}(:status=>0, :message=>"Changed description")

  try
    @assert length(@params(:POST)) >= 2 "Keys numbers"

    session = Genie.Sessions.session(@params)
    @assert GenieAuthentication.is_authenticated(session) "Not logged in"

    user_id = Genie.Sessions.get(session, :__auth_user_id)
    pids = SearchLight.query("SELECT * FROM pids_by_uid($user_id)")
    @assert parse(UInt, @params(:pid)) in pids[:, :pid] "Wrong pid"

    profile = SearchLight.findone(Profile, id = @params(:pid))
    profile.description = @params(:description)
    save!(profile)
  catch ex
    @match ex begin
      AssertionError("Keys numbers") =>
        begin 
          response[:status] = 101
          response[:message] = "Wrong keys numbers"
        end

      KeyError(:description) =>
        begin 
          response[:status] = 102
          response[:message] = "No description key"
        end

      AssertionError("Not logged in") =>
        begin 
          response[:status] = 103
          response[:message] = "Not logged in"
        end
      AssertionError("Wrong pid") =>
        begin 
          response[:status] = 104
          response[:message] = "Wrong pid"
        end
      KeyError(:pid) =>
        begin 
          response[:status] = 105
          response[:message] = "No pid key"
        end
    end
  finally
    return json(response)
  end
end

function getAllProfiles()
  profile = nothing

  response = Dict{Symbol, Any}(:status=>0, :profiles=>[], :message=>"Got profiles")

  try
    for profile in all(Profile)
      push!(response[:profiles], [string(profile.id), profile.name, profile.description])
    end
  catch ex
    println(ex)
  finally
    return json(response)
  end
end

end
