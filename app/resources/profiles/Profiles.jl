module Profiles

import SearchLight: AbstractModel, DbId, Validation
import Base: @kwdef
using ProfilesValidator

export Profile

@kwdef mutable struct Profile <: AbstractModel
  id::DbId = DbId()
  name::String = ""
  description::String = ""
end

Validation.validator(u::Type{Profile}) = Validation.ModelValidator([
  Validation.ValidationRule(:name, ProfilesValidator.not_empty)
])

end
