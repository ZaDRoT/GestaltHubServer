module GestaltHubServer

using Genie, Logging, LoggingExtras

function main()
  Base.eval(Main, :(const UserApp = GestaltHubServer))

  Genie.genie(; context = @__MODULE__)

  Base.eval(Main, :(const Genie = GestaltHubServer.Genie))
  Base.eval(Main, :(using Genie))
end

end
